const BASE_URL = "https://6375abb5b5f0e1eb85f64c52.mockapi.io";

const CART_LIST = "CART_LIST";
var productCartList = [];

// var firstTimePush = 0;

function fetchAllToDo() {
  axios({
    url: `${BASE_URL}/Cellphones_list`,
    method: "GET",
  })
    .then(function (res) {
      //   renderToDoList(res.data);
      console.log("res: ", res);
      console.table(res.data);
      renderProductList(res.data);
      //   turnOffLoading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      //   turnOffLoading();
    });
}
fetchAllToDo();

function saveToLocalStorage() {
  let jsonCartList = JSON.stringify(productCartList);
  localStorage.setItem(CART_LIST, jsonCartList);
}

console.log("productCartList test: ", productCartList);
var dataJson = localStorage.getItem(CART_LIST);
console.log("dataJson: ", dataJson);

if (dataJson !== null) {
  var productCartList = JSON.parse(dataJson);

  // console.log("productCartList 1: ", productCartList);
  renderProductCart(productCartList);
}

function optionSamsung() {
  // location.href = "../mobilePage/samsung.html"

  var samsungList = [];
  axios({
    url: `${BASE_URL}/Cellphones_list`,
    method: "GET",
  })
    .then(function (res) {
      res.data.forEach((item) => {
        if (item.brand == "Samsung") {
          samsungList.push(item);
        }
      });
      console.log("samsungList: ", samsungList);
      renderProductList(samsungList);
    })
    .catch(function (err) {
      console.log("err: ", err);
      //   turnOffLoading();
    });
}
function optionIphone() {
  // location.href = "../mobilePage/samsung.html"
  var iphoneList = [];
  axios({
    url: `${BASE_URL}/Cellphones_list`,
    method: "GET",
  })
    .then(function (res) {
      res.data.forEach((item) => {
        if (item.brand == "Iphone") {
          iphoneList.push(item);
        }
      });
      renderProductList(iphoneList);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function addToCart(id) {
  sideNav(1);

  axios({
    url: `${BASE_URL}/Cellphones_list/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log("productCartList: ", productCartList);
      if (productCartList.length == 0) {
        product1stAddToCart(productCartList, res.data);
      } else {
        var duplicateItem = productDuplicateCheck(productCartList, res.data);
        console.log("duplicateItem : ", duplicateItem);
        if (duplicateItem.isDuplicate !== true) {
          product1stAddToCart(productCartList, res.data);
        } else {
          productCartList[duplicateItem.indexDuplicate].quantity++;
        }
      }

      saveToLocalStorage();
      renderProductCart(productCartList);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function deleteItemFromCart(id) {
  var indexOfProductID = getIndexFromList(productCartList, id);
  productCartList.splice(indexOfProductID, 1);
  console.log("productCartList del: ", productCartList);
  renderProductCart(productCartList);
  saveToLocalStorage();
}

function decreaseNosOfCart(id) {
  var indexOfProductID = getIndexFromList(productCartList, id);
  var itemQuantity = productCartList[indexOfProductID].quantity;
  if (itemQuantity > 0) {
    productCartList[indexOfProductID].quantity--;
  }
  saveToLocalStorage();
  renderProductCart(productCartList);
}
function increaseNosOfCart(id) {
  var indexOfProductID = getIndexFromList(productCartList, id);
  // var itemQuantity = productCartList[indexOfProductID].quantity;
  productCartList[indexOfProductID].quantity++;
  saveToLocalStorage();
  renderProductCart(productCartList);
}

function deleteAllCart() {
  renderPaymentScreen(productCartList);
  paymentPopup(1);
  productCartList = [];
  saveToLocalStorage();
}
console.log("productCartList Final: ", productCartList);

function orderNow() {
  productCartList = [];
  saveToLocalStorage();
  renderProductCart(productCartList);
}
