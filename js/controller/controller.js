function renderProductList(data) {
  var contentHTML = "";
  data.forEach((item) => {
    var contentTr = `
    <div class="col-md-3">
    <div class="card mb-4 shadow-sm">
      <div class="card-body p-0">
        <div class="product_item">
          <div class="product_img pt-3">
            <img class="w-100" src="${item.image}" alt="" />
          </div>
         
          <div class="product_info">
            <p class="card-text mb-1">Vi xử lý: ${item.chip}</p>
            <p class="card-text mb-1">Ram: ${item.ram} GB</p>
            <p class="card-text mb-1">Dung lượng: ${item.capacity} GB</p>
            <p class="card-text mb-1">Camera trước: ${item.frontCam} MP</p>
            <p class="card-text mb-1">Camera sau: ${item.rearCam} MP</p>
          </div>
        </div>
        <div class="product_footer p-3">
          <div className="product_name">
            <p class="card-text mb-2">${item.name} ${item.capacity}GB</p>
          </div>
          
            <div class="product_price">
              <h5 class="card-text text-danger font-weight-bold">$${item.price}</h5>
            </div>
            <div class="product_cart d-flex justify-content-end">
              <button onclick="addToCart(${item.id})" class="btn btn-light border">
              <i class="fa-solid fa-cart-plus"></i>
              </button>
            </div>
          
        </div>
      </div>
    </div>
  </div>
  `;

    contentHTML += contentTr;
  });
  document.querySelector(".product_view").innerHTML = contentHTML;
}
function product1stAddToCart(productCartList, product) {
  var addCartItem = new cartItem(
    product.id,
    product.image,
    product.name,
    product.price,
    product.capacity,
    1
  );
  productCartList.push(addCartItem);
}

function productDuplicateCheck(productCartList, productList) {
  for (let index = 0; index < productCartList.length; index++) {
    if (productCartList[index].id == productList.id) {
      return {
        isDuplicate: true,
        indexDuplicate: index,
      };
    }
  }
  return {
    isDuplicate: false,
  };

  // false;
}

function totalPayment(productCartList) {
  var totalPayment = 0;
  for (let index = 0; index < productCartList.length; index++) {
    totalPayment +=
      productCartList[index].price * productCartList[index].quantity;
  }
  return totalPayment;
}

function renderProductCart(cartList) {
  var contentHTML = "";
  // console.log("cartList: ", cartList);

  if (cartList.length !== 0) {
    cartList.forEach((item) => {
      console.log("item: ", item);
      var contentTr = `
  <div class="row mb-3">
    <div class="col-3 pt-2">
      <img src="${item.image}" class= "w-100" alt="" />
    </div>
    <div class="col-9">
      <p class = "mb-2" >${item.name}</p>
      <p class = "cardText_capacity text-secondary mb-2">${item.capacity}GB</p>
      <p class ="text-secondary mb-2">$${item.price}</p>
      <div class = "row mx-0">
        <button onclick="decreaseNosOfCart(${item.id})"
        class="cartBtn">-</button>
      <div class="d-flex align-items-center px-2 cartQuantity">
        <p class="mb-0">${item.quantity}</p>
      </div> 
      <button onclick="increaseNosOfCart(${item.id})" class="cartBtn"> + </button>
      <button onclick="deleteItemFromCart(${item.id})"
      class="cartBtn ml-3"><i class="fa fa-trash-alt"></i></button>
    </div>
    </div>
  </div>
      `;
      contentHTML += contentTr;
      document.querySelector(".product_cartContent").innerHTML = contentHTML;

      var totalPaymentCart = totalPayment(productCartList);
      var contentPayment = `
      <p class="col-4 mb-0" >Total:</p>
      <p class="col-4 mb-0">$ ${totalPaymentCart}</p>
      `;
      document.querySelector(".product_payment").innerHTML = contentPayment;

      var contentThanhToan = `
      <button onclick="deleteAllCart()" class="btn btn-success">Thanh Toán</button>
      `;
      document.querySelector(".product_paymentBtn").innerHTML =
        contentThanhToan;
    });
  } else {
    document.querySelector(".product_cartContent").innerHTML = "";
    document.querySelector(".product_payment").innerHTML = "";
    document.querySelector(".product_paymentBtn").innerHTML = "";
  }
}

function getIndexFromList(productCartList, id) {
  for (let index = 0; index < productCartList.length; index++) {
    var item = productCartList[index];
    if (item.id == id) {
      return index;
    }
  }
}

function renderPaymentScreen(productCartList) {
  var contentPaymentAll = `
 
<p>Đã thanh toán thành công số tiền: </h3>
<h5 class = "text-secondary mb-3" >$ ${totalPayment(productCartList)}</h5>
<h4>HẸN GẶP LẠI QUÝ KHÁCH!</h3>


  `;

  // var contentButtonOrder = `

  // <button onclick="orderNow()" class="btn btn-danger">Order Now</button>

  // `;
  document.querySelector(".paymentScreen").innerHTML = contentPaymentAll;
  // document.querySelector(".product_btnOrder").innerHTML = contentButtonOrder;
}

function sideNav(e) {
  let t = document.getElementsByClassName("side-nav")[0];
  let n = document.getElementsByClassName("cover")[0];

  (t.style.right = e ? "0" : "-100%"), (n.style.display = e ? "block" : "none");
  // CartIsEmpty();
}
function paymentPopup(e) {
  sideNav(0);
  let t = document.getElementsByClassName("product_paymentScreen")[0];
  console.log("t: ", t);
  let n = document.getElementsByClassName("cover")[0];

  (t.style.width = e ? "30%" : "0%"), (n.style.display = e ? "block" : "none");
  // CartIsEmpty();
}

// paymentPopup(0);
/**
 * [
 {
  "name": "iphoneX",
  "price": "1000",
  "screen": "screen 68",
  "backCamera": "2 camera 12 MP",
  "frontCamera": "7 MP",
  "img": "https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-hh-600x600.jpg",
  "desc": "Thiết kế mang tính đột phá",
  "type": "iphone"
 },
 */
