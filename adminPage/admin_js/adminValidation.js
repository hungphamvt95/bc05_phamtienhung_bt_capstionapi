function nullValid(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message, "initial");
    return false;
  } else {
    showMessageErr(idErr, "", "none");
    return true;
  }
}

function duplicateValid(userInput, staffList, idErr, message) {
  let index = getIndexFromProductList(userInput, staffList);
  if (index !== -1) {
    showMessageErr(idErr, message, "initial");
    return false;
  } else {
    showMessageErr(idErr, "", "none");
    return true;
  }
}

function isNumValid(value, idErr, message) {
  var reg = /^\d*$/;
  let isNumber = reg.test(value);
  if (isNumber) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function isTextValid(text, idErr, message) {
  var reg = /^[A-Za-z][A-Za-z\s]*$/;
  let isText = reg.test(text);
  if (isText) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}
function isNot1stNumValid(text, idErr, message) {
  var reg = /[a-zA-Z][a-zA-Z0-9]*/;
  let isText = reg.test(text);
  if (isText) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function isPriceValid(userInput, idErr, message) {
  if (userInput >= 100 && userInput <= 2500) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function isCamPixelValid(userInput, idErr, message) {
  if (userInput >= 1 && userInput <= 50) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function validateData(data) {
  // kiểm tra tên hợp lệ
  var isValidName =
    nullValid(data.name, "invalidName", "Tên SP không để trống") &&
    isNot1stNumValid(data.name, "invalidName", "Tên SP phải bắt đầu bằng chữ");
  // kiểm tra loại hợp lệ
  var isValidBrand = nullValid(data.brand, "invalidBrand", "Chọn loại SP");
  // kiểm tra giá hợp lệ
  var isValidPrice = isPriceValid(
    data.price,
    "invalidPrice",
    "Nhập giá tiền từ $100 - $2500"
  );
  // kiểm tra hình sản phẩm hợp lệ
  var isValidImg = nullValid(
    data.image,
    "invalidImage",
    "Nhập đường link hình ảnh"
  );
  //kiểm tra hợp lệ vi xử lý
  var isValidChip = nullValid(
    data.chip,
    "invalidChip",
    "Vi xử lý không được để trống"
  );
  // kiểm tra hợp lệ RAM
  var isValidRam = nullValid(data.ram, "invalidRam", "Chọn dung lượng RAM");
  // kiểm tra hợp lệ dung lượng bộ nhớ
  var isValidCapacity = nullValid(
    data.capacity,
    "invalidCap",
    "Chọn dung lượng bộ nhớ"
  );
  // kiểm tra hợp lệ cam trước
  var isValidFrontCam =
    nullValid(data.frontCam, "invalidFrontCam", "Không được để trống") &&
    isNumValid(data.frontCam, "invalidFrontCam", "Vui lòng nhập số");

  // kiểm tra hợp lệ cam sau
  var isValidRearCam =
    nullValid(data.rearCam, "invalidRearCam", "Không được để trống") &&
    isNumValid(data.rearCam, "invalidRearCam", "Vui lòng nhập số");

  if (
    isValidName &&
    isValidBrand &&
    isValidPrice &&
    isValidImg &&
    isValidChip &&
    isValidRam &&
    isValidCapacity &&
    isValidFrontCam &&
    isValidRearCam
  ) {
    return true;
  } else {
    return false;
  }
}
