const BASE_URL = "https://6375abb5b5f0e1eb85f64c52.mockapi.io";
var idEdited = null;
var productListForValid = null;
document.getElementById("btnThemSP").disabled = false;
document.getElementById("btnCapNhat").disabled = false;

let fetchAllProductList = () => {
  axios({
    url: `${BASE_URL}/Cellphones_list`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      console.log(res);
      renderFoodList(res.data);
      productListForValid = res.data;
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
};
fetchAllProductList();

function addProduct() {
  // console.log("productListForValid: ", productListForValid);
  var productInput = getInfoFromForm();
  // console.log("productInput: ", productInput);

  // nếu tất cả các trường nhập hợp lệ
  if (validateData(productInput)) {
    turnOnLoading();
    axios({
      url: `${BASE_URL}/Cellphones_list`,
      method: "POST",
      data: productInput,
    })
      .then((res) => {
        console.log(res);
        fetchAllProductList();
      })
      .catch((err) => {
        turnOffLoading();
        console.log(err);
      });
  }
}

function deleteProduct(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/Cellphones_list/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      fetchAllProductList();
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
  console.log("id: ", id);
}

function editProduct(id) {
  document.getElementById("btnCapNhat").disabled = false;
  document.getElementById("btnThemSP").disabled = true;
  axios({
    url: `${BASE_URL}/Cellphones_list/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      showInfoToForm(res.data);
      idEdited = res.data.id;
    })
    .catch((err) => {
      console.log(err);
    });
}
function updateProduct() {
  let data = getInfoFromForm();
  if (validateData(data)) {
    turnOnLoading();
    axios({
      url: `${BASE_URL}/Cellphones_list/${idEdited}`,
      method: "PUT",
      data: data,
    })
      .then(function (res) {
        resetForm();
        $("#closeBtn").click();
        console.log("res: ", res);
        fetchAllProductList();
      })
      .catch(function (err) {
        turnOffLoading();
        console.log("err: ", err);
      });
  }
}

function detailInfo(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/Cellphones_list/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      showDetailSpec(res.data);
      turnOffLoading();
    })
    .catch((err) => {
      console.log(err);
    });
}

// {
//  "id": "1",
//  "name": "Iphone X",
//  "price": "1000",
//  "screen": "screen 68",
//  "backCamera": "2 camera 12 MP",
//  "frontCamera": "7 MP",
//  "img": "https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-hh-600x600.jpg",
//  "desc": "Thiết kế mang tính đột phá",
//  "type": "Iphone"
// },
