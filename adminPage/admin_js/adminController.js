renderFoodList = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let { id, name, price, brand, image } = item;

    contentHTML += `
      <tr>
          <td class="align-middle">${id}</td>
          <td class="align-middle"><img class="product_image" src="${image}" alt="" /></td>
          <td class="align-middle">${name}</td>
          <td class="align-middle">${brand}</td>
          <td class="align-middle">
          <button onclick="detailInfo(${id})" class = "btn btn-success">Chi tiết</button>
          </td>
          <td class="align-middle">${price}</td>
          <td class="align-middle">
          <button onclick = "deleteProduct(${id})" class = "btn btn-danger">Xóa</button>
          <button onclick = "editProduct(${id})"class = "btn btn-secondary" data-toggle="modal"
          data-target="#exampleModal">Sửa</button>
          </td>
      </tr>`;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

function getInfoFromForm() {
  // let id = document.getElementById("productID").value;
  let name = document.getElementById("productName").value;
  let brand = document.getElementById("productBrand").value;
  let price = document.getElementById("productPrice").value;
  let image = document.getElementById("productImg").value;
  let chip = document.getElementById("productChip").value;
  let ram = document.getElementById("productRam").value;
  let capacity = document.getElementById("productCapacity").value;
  let frontCam = document.getElementById("productFrontCam").value;
  let rearCam = document.getElementById("productRearCam").value;

  return {
    name,
    brand,
    price,
    image,
    chip,
    ram,
    capacity,
    frontCam,
    rearCam,
  };
}

function getIndexFromProductList(id, productList) {
  for (let index = 0; index < productList.length; index++) {
    if (productList[index].id == id) {
      return index;
    }
  }
  return -1;
}

function showInfoToForm(data) {
  let { name, brand, price, image, chip, ram, capacity, frontCam, rearCam } =
    data;
  // document.getElementById("productID").value = id;
  document.getElementById("productName").value = name;
  document.getElementById("productBrand").value = brand;
  document.getElementById("productPrice").value = price;
  document.getElementById("productImg").value = image;
  document.getElementById("productChip").value = chip;
  document.getElementById("productRam").value = ram;
  document.getElementById("productCapacity").value = capacity;
  document.getElementById("productFrontCam").value = frontCam;
  document.getElementById("productRearCam").value = rearCam;
}

function showDetailSpec(data) {
  var contentDetailHTML = "";
  let { name, brand, price, image, chip, ram, capacity, frontCam, rearCam } =
    data;
  contentDetailHTML = `
    <div class="card" style="width: 100%;">
    <div class="mt-3 d-flex justify-content-center">
    <img src=${image} class="card-img-top" alt="">
    </div>

    <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <p class="card-text mb-2">Vi xử lý: ${chip}</p>
      <p class="card-text mb-2">RAM: ${ram} GB</p>
      <p class="card-text mb-2">Dung Lượng: ${capacity} GB</p>
      <p class="card-text mb-2">Camera Trước: ${frontCam} MP</p>
      <p class="card-text mb-2">Camera Sau: ${rearCam} MP</p>
     </div>
    </div>
   `;

  document.getElementById("detailSpec").innerHTML = contentDetailHTML;
}

function resetForm() {
  document.getElementById("productForm").reset();
}

function showMessageErr(idErr, message, showErr) {
  document.getElementById(idErr).style.display = showErr;
  document.getElementById(idErr).innerHTML = message;
}

document.getElementById("btnThem").addEventListener("click", function () {
  document.getElementById("btnThemSP").disabled = false;
  document.getElementById("btnCapNhat").disabled = true;

});

function turnOnLoading() {
  document.querySelector(".loading_screen").style.display = "flex";
}

function turnOffLoading() {
  document.querySelector(".loading_screen").style.display = "none";
}
//  "id": "1",
//  "name": "Iphone X",
//  "price": "1000",
//  "screen": "screen 68",
//  "backCamera": "2 camera 12 MP",
//  "frontCamera": "7 MP",
//  "img": "https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-hh-600x600.jpg",
//  "desc": "Thiết kế mang tính đột phá",
//  "type": "Iphone"
// },
